<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Inline Elements Example - Airport</title>
  <style>
    p{
      padding:          10px;
      background-color: lightblue;
      font-size:        40px;
      color:            white;
    }
    #one{
      text-align: right;
    } 
    #two{
      text-align: center;
    }
    #three{
      text-align: left;
    }
    .a1{
      margin-right: 50px;
    }
    .a2{
      margin-left: 50px;

    }
    .a3{
      margin-left: 100px;
      
    }
    .a4:hover{
      font-size: 80px;
      color: black;

    }
    .a5{
      margin-right: 100px;

    }
    .a6{
      margin-right: 50px;

    }
    .a7{
      margin-left: 50px;

    }
  </style>
</head>
<body>

  <p id="one">
    <span class="a1">&#9992;</span>
    <span class="a2">&#9992;</span>
  </p>
  <p id="two">
    <span class="a3">&#9992;</span><br>
    <span class="a4">&#9992;</span><br>
    <span class="a5">&#9992;</span>
  </p>
  <p id="three">
    <span class="a6">&#9992;</span>
    <span class="a7">&#9992;</span>
  </p>


</body>
</html>